import errors.MatrixNotSquareException;
import errors.MatrixOperationException;

import java.util.List;

public interface Matrix<T extends Number> {
  Matrix<T> add(Matrix<T> matrix) throws MatrixOperationException;
  Matrix<T> sub(Matrix<T> matrix) throws MatrixOperationException;
  Matrix<T> mul(Matrix<T> matrix) throws MatrixOperationException;
  Matrix<T> mul(List<T> matrix) throws MatrixOperationException;
  Matrix<T> mul(T number) throws MatrixOperationException;

  Matrix<T> transparent();
  Matrix<T> inverse() throws MatrixOperationException;

  T getMinor(int row, int col) throws MatrixNotSquareException;
  T calculateDeterminate() throws MatrixNotSquareException;

  int getRowCount();
  int getColumnCount();

  List<T> getRow(int index) throws IndexOutOfBoundsException;
  List<T> getColumn(int index) throws IndexOutOfBoundsException;
  T getElement(int row, int col) throws IndexOutOfBoundsException;
  void setElement(int row, int col, T element);
}
