package errors;

import java.lang.Exception;

public class MatrixOperationException extends Exception {
   public MatrixOperationException(String errorMessage) {
      super(errorMessage);
   }
}
