package errors;

public class MatrixDetException extends MatrixOperationException {
   public MatrixDetException(String errorMessage) {
      super(errorMessage);
   }
}
