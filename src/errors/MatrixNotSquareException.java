package errors;

public class MatrixNotSquareException extends MatrixOperationException {
   public MatrixNotSquareException(String errorMessage) {
      super(errorMessage);
   }
}
