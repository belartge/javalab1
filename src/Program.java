
public class Program {

   public static void main(String[] args) {
      Test test = new Test();

      test.testAdd();
      test.testSub();
      test.testMulToAnotherMatrix();
      test.testMulToVector();
      test.testMulToNumber();
      test.testMinor();
      test.testDet();
      test.testInverse();
   }
}
