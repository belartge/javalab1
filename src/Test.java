import errors.MatrixOperationException;

import java.util.ArrayList;
import java.util.List;

public class Test {
   private Matrix<Double> matrix;
   private Matrix<Double> anotherMatrix;

   private List<Double> vector;
   private Double number;

   public Test() {
      ArrayList<Double> l0 = new ArrayList<>();
      ArrayList<Double> l1 = new ArrayList<>();
      ArrayList<Double> r0 = new ArrayList<>();
      ArrayList<Double> r1 = new ArrayList<>();
      l0.add(1.0);
      l0.add(1.0);
      l1.add(1.0);
      l1.add(1.0);

      r0.add(7.0);
      r0.add(1.0);
      r1.add(1.0);
      r1.add(3.0);


      List<List<Double>> l = new ArrayList<>();
      l.add(l0);
      l.add(l1);

      List<List<Double>> r = new ArrayList<>();
      r.add(r0);
      r.add(r1);

      matrix = new MatrixImpl(l);
      anotherMatrix = new MatrixImpl(r);

      vector = new ArrayList<>();
      vector.add(3.0);
      vector.add(4.0);

      number = 5.0;
   }

   void printLeft() {
      System.out.println("Left: ");
      System.out.println(matrix.toString());
   }

   void printRight() {
      System.out.println("Right: ");
      System.out.println(anotherMatrix.toString());
   }


   void testAdd() {
      try {
         System.out.println("Add: ");
         System.out.println(matrix.add(anotherMatrix).toString());
      } catch (MatrixOperationException ex) {
         ex.printStackTrace();
      }
   }

   void testSub() {
      try {
         System.out.println("Sub: ");
         System.out.println(matrix.sub(anotherMatrix).toString());
      } catch (MatrixOperationException ex) {
         ex.printStackTrace();
      }
   }

   void testMulToAnotherMatrix() {
      try {
         System.out.println("Mul to another matrix: ");
         System.out.println(matrix.mul(anotherMatrix).toString());
      } catch (MatrixOperationException ex) {
         ex.printStackTrace();
      }
   }

   void testMulToVector() {
      try {
         System.out.println("Mul to vector: ");
         System.out.println(matrix.mul(vector).toString());
      } catch (MatrixOperationException ex) {
         ex.printStackTrace();
      }
   }

   void testMulToNumber() {
      try {
         System.out.println("Mul to number: ");
         System.out.println(matrix.mul(number).toString());
      } catch (MatrixOperationException ex) {
         ex.printStackTrace();
      }
   }

   void testMinor() {
      try {
         System.out.println("Minor of left matrix at 0,0: ");
         System.out.println(matrix.getMinor(0,0).toString());
      } catch (MatrixOperationException ex) {
         ex.printStackTrace();
      }
   }

   void testDet() {
      try {
         System.out.println("Determinant: ");
         System.out.println(anotherMatrix.calculateDeterminate().toString());
      } catch (MatrixOperationException ex) {
         ex.printStackTrace();
      }
   }

   void testInverse() {
      try {
         System.out.println("Inversed: ");
         System.out.println(matrix.inverse().toString());
      } catch (MatrixOperationException ex) {
         ex.printStackTrace();
      }
   }


}
