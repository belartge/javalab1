import errors.MatrixDetException;
import errors.MatrixNotSquareException;
import errors.MatrixOperationException;

import java.util.ArrayList;
import java.util.List;

public class MatrixImpl implements Matrix<Double> {
   private List<List<Double>> matrix;

   public MatrixImpl(int cols, int rows) {
      matrix = new ArrayList<>();

      for (int i = 0; i <= rows; i++) {
         List<Double> row = new ArrayList<>();

         for (int j = 0; j <= cols; j++) {
            row.add(0.0);
         }

         matrix.add(row);
      }
   }

   MatrixImpl(List<List<Double>> matrix) {
      this.matrix = new ArrayList<>();

      for (List<Double> vector : matrix) {
         this.matrix.add(new ArrayList<>(vector));
      }
   }

   @Override
   public Matrix<Double> add(Matrix<Double> matrix) throws MatrixOperationException {
      if (getRowCount() != matrix.getRowCount() || getColumnCount() != matrix.getColumnCount()) {
         throw new MatrixOperationException("Матрицы разных размеров ");
      }

      MatrixImpl resultMatrix = new MatrixImpl(getRowCount() - 1, getColumnCount() - 1);

      for (int i = 0; i < getRowCount(); i++) {
         for (int j = 0; j < getColumnCount(); j++) {
            resultMatrix.setElement(i, j, matrix.getElement(i, j) + getElement(i, j));
         }
      }

      return resultMatrix;
   }

   @Override
   public Matrix<Double> sub(Matrix<Double> matrix) throws MatrixOperationException {
      if (getRowCount() != matrix.getRowCount() || getColumnCount() != matrix.getColumnCount()) {
         throw new MatrixOperationException("Матрицы разных размеров");
      }


      MatrixImpl resultMatrix = new MatrixImpl(getRowCount() - 1, getColumnCount() - 1);

      for (int i = 0; i < getRowCount(); i++) {
         for (int j = 0; j < getColumnCount(); j++) {
            resultMatrix.setElement(i, j, matrix.getElement(i, j) - getElement(i, j));
         }
      }

      return resultMatrix;
   }

   @Override
   public Matrix<Double> mul(Matrix<Double> matrix) throws MatrixOperationException {
      if (getColumnCount() != matrix.getRowCount()) {
         throw new MatrixOperationException("Матрицы разных размеров");
      }

      MatrixImpl resultMatrix = new MatrixImpl(getRowCount() - 1, getColumnCount() - 1);

      for (int i = 0; i < getRowCount(); i++) {
         for (int j = 0; j < getColumnCount(); j++) {
            double value = 0.0;
            for (int r = 0; r < getColumnCount(); r++) {
               value += getElement(i, r) * getElement(r, j);
            }
            resultMatrix.setElement(i, j, value);
         }
      }

      return resultMatrix;
   }

   @Override
   public Matrix<Double> mul(List<Double> vector) throws MatrixOperationException {
      if (matrix == null || getRowCount() == 0 || getColumnCount() == 0) {
         throw new MatrixOperationException("Матрица пуста");
      }
      if (vector == null) {
         throw new MatrixOperationException("Вектор пуст");
      }
      if (getColumnCount() != vector.size()) {
         throw new MatrixOperationException("Матрица и вектор разного размера");
      }

      MatrixImpl resultMatrix = new MatrixImpl(getRowCount() - 1, getColumnCount() - 1);

      for (int i = 0; i < getRowCount(); i++) {
         for (int j = 0; j < getColumnCount(); j++) {
            double value = 0.0;
            for (int r = 0; r < getColumnCount(); r++) {
               value += getElement(i, r) * vector.get(r);
            }
            resultMatrix.setElement(i, j, value);
         }
      }

      return resultMatrix;
   }

   @Override
   public Matrix<Double> mul(Double number) throws MatrixOperationException {
      if (matrix == null || getRowCount() == 0 || getColumnCount() == 0) {
         throw new MatrixOperationException("Матрица пуста");
      }

      MatrixImpl resultMatrix = new MatrixImpl(getRowCount() - 1, getColumnCount() - 1);

      for (int i = 0; i < getRowCount(); i++) {
         for (int j = 0; j < getColumnCount(); j++) {
            resultMatrix.setElement(i, j, number * getElement(i, j));
         }
      }

      return resultMatrix;
   }

   @Override
   public Matrix<Double> transparent() {
      List<List<Double>> newMatrix = new ArrayList<>();
      for (int i = 0; i < getColumnCount(); i++) {
         newMatrix.add(getColumn(i));
      }
      return new MatrixImpl(newMatrix);
   }

   private Matrix<Double> getMinoredMatrix() throws MatrixNotSquareException {
      List<List<Double>> minorListForMatrix = new ArrayList<>();

      for (int i = 0; i < getRowCount(); i++) {
         List<Double> newRow = new ArrayList<>();
         for (int j = 0; j < getColumnCount(); j++) {
            newRow.add(getMinor(i, j));
         }
         minorListForMatrix.add(newRow);
      }
      return new MatrixImpl(minorListForMatrix);
   }

   private Matrix<Double> getAdditionMatrix() throws MatrixNotSquareException {
      List<List<Double>> minorListForMatrix = new ArrayList<>();

      for (int i = 0; i < getRowCount(); i++) {
         List<Double> newRow = new ArrayList<>();
         for (int j = 0; j < getColumnCount(); j++) {
            newRow.add(getSignedMinor(i, j));
         }
         minorListForMatrix.add(newRow);
      }
      return new MatrixImpl(minorListForMatrix);
   }

   @Override
   public Matrix<Double> inverse() throws MatrixOperationException {
      if (calculateDeterminate() == 0) {
         throw new MatrixDetException("Определитель матрицы равен 0");
      }

      Matrix<Double> minors = getMinoredMatrix();
      Matrix<Double> additions = getAdditionMatrix();

      return additions.transparent().mul(minors.calculateDeterminate());
   }

   private Matrix<Double> getDopolnenie(int row, int col) {
      List<List<Double>> newMatrix = new ArrayList<>();

      int length = Math.min(getRowCount(), getColumnCount()) - 1;

      double[][] result = new double[length][length];
      for (int i = 0; i < length; i++)
         for (int j = 0; j < length; j++) {
            if (i < row && j < col) {
               result[i][j] = getElement(i, j);
            } else if (i >= row && j < col) {
               result[i][j] = getElement(i + 1, j);
            } else if (i < row && j >= col) {
               result[i][j] = getElement(i, j + 1);
            } else { //i>x && j>y
               result[i][j] = getElement(i + 1, j + 1);
            }
         }

      for (int i = 0; i < length; i++) {
         List<Double> newRow = new ArrayList<>();
         for (int j = 0; j < length; j++) {
            newRow.add(result[i][j]);
         }

         newMatrix.add(newRow);
      }
      return new MatrixImpl(newMatrix);
   }

   @Override
   public Double getMinor(int row, int col) throws MatrixNotSquareException {

      if (getColumnCount() != getRowCount()) {
         throw new MatrixNotSquareException("Матрица не квадратная");
      }

      return getDopolnenie(row, col).calculateDeterminate();
   }

   private Double getSignedMinor(int row, int col) throws MatrixNotSquareException {
      if (getColumnCount() != getRowCount()) {
         throw new MatrixNotSquareException("Матрица не квадратная");
      }

      return getMinor(row, col) * Math.pow(-1, row + col);
   }

   @Override
   public Double calculateDeterminate() throws MatrixNotSquareException {
      if (getColumnCount() != getRowCount()) {
         throw new MatrixNotSquareException("Матрица не квадратная");
      }

      int length = Math.min(getRowCount(), getColumnCount());

      if (length == 1) {
         return getElement(0, 0);
      }
      int sign = 1;
      double sum = 0;
      for (int i = 0; i < length; i++) {
         sum += sign * getElement(0, i) * getMinor(0, i);
         sign *= -1;
      }
      return sum;
   }

   @Override
   public int getRowCount() {
      return matrix.size();
   }

   @Override
   public int getColumnCount() {
      if (matrix.size() > 0) {
         return matrix.get(0).size();
      }
      return 0;
   }

   @Override
   public List<Double> getRow(int index) throws IndexOutOfBoundsException {
      return matrix.get(index);
   }

   @Override
   public List<Double> getColumn(int index) throws IndexOutOfBoundsException {
      List<Double> column = new ArrayList<>();
      for (List<Double> row : matrix) {
         column.add(row.get(index));
      }
      return column;
   }

   @Override
   public Double getElement(int row, int col) throws IndexOutOfBoundsException {
      return matrix.get(row).get(col);
   }

   @Override
   public void setElement(int row, int col, Double element) throws IndexOutOfBoundsException {
      if (getRowCount() <= row) {
         throw new IndexOutOfBoundsException("Нет такой строки");
      }
      if (getColumnCount() <= col) {
         throw new IndexOutOfBoundsException("Нет такого столбца");
      }

      matrix.get(row).set(col, element);
   }

   @Override
   public String toString() {
      String space = "  \t  ";
      StringBuilder stringBuilder = new StringBuilder();
      for (List<Double> row : matrix) {
         for (Double element : row) {
            stringBuilder.append(element);
            stringBuilder.append(space);
         }
         stringBuilder.append('\n');
      }
      return stringBuilder.toString();
   }
}
